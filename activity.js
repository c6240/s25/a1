db.fruits.aggregate(
	[
			{$match: {"onSale": true}},
			{$count: "fruitsOnSale"}
	])

db.fruits.aggregate(
	[
			{$match: {"stock": {$gte: 20}}},
			{$count: "enoughStock"}
	])

db.fruits.aggregate(
	[
		{$match: {"onSale": true}},
		{
			$group: {_id:"null", avg_price: {$avg: "$price"}}
		}
	])

db.fruits.aggregate(
	[
		{$match: {"onSale": true}},
		{
			$group: {_id:"null", max_price: {$max: "$price"}}
		}
	])

db.fruits.aggregate(
	[
		{$match: {"onSale": true}},
		{
			$group: {_id:"null", min_price: {$min: "$price"}}
		}
	])